from flask import Flask
from flask_restful import Api
from resources.restaurants_resource import RestaurantsResource, RESTAURANTS_ENDPOINT
import logging


def create_app():
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%m-%d %H:%M',
        handlers=[logging.FileHandler('log/log.txt'), logging.StreamHandler()],
    )

    app = Flask(__name__)
    api = Api(app)
    api.add_resource(RestaurantsResource, RESTAURANTS_ENDPOINT)
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
