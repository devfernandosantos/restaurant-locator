## Restaurant Locator


### Note: 

Instead of finding 50, I limited the search to 40 restaurants to increase performance. 


### Instructions:


### Create a virtual environment (virtualenv) and run the command:

```pip install -r requirements.txt```


### Run the tests using the command:

```pytest```


### Run the program using the command:

```python api.py```



### GET

http://localhost:5000/api/v1/restaurants?lat=&long=&city=

#### lat = latitude
#### long = longitude
#### city = city

###

#### Example 1:

http://localhost:5000/api/v1/restaurants?lat=40.76765165793299&long=-73.98738074296989&city=New York

#### Example 2:

http://localhost:5000/api/v1/restaurants?lat=40.402937365278504&long=-3.699257687683903&city=Madrid

#### Example 3:

http://localhost:5000/api/v1/restaurants?lat=53.32904141711612&long=-6.262355993169992&city=Dublin 
