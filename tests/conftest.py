import pytest

from os import path
import sys

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

from api import create_app


@pytest.fixture()
def app():
    app = create_app()
    app.config.update({
        "TESTING": True,
    })

    yield app
