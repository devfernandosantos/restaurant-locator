from resources.restaurants_resource import RESTAURANTS_ENDPOINT

lat, long, city = 40.7168127952275, -74.00586288895502, 'New York'


def test_get_restaurants(client):
    response = client.get(f'{RESTAURANTS_ENDPOINT}?lat={lat}&long={long}&city={city}')
    assert response.status_code == 200


def test_get_restaurants_missing_arguments(client):
    response = client.get(f'{RESTAURANTS_ENDPOINT}?lat={lat}&long={long}')
    assert response.status_code == 400


def test_get_restaurants_invalid_request(client):
    response = client.get(f'{RESTAURANTS_ENDPOINT}?lat=22222265{lat}&long={long}&city={city}')
    assert response.status_code == 500
