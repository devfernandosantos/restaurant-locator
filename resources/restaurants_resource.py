import logging
import time

import googlemaps
from decouple import config
from flask import request
from flask_restful import Resource, abort
from utils.pagination import Pagination
from werkzeug.exceptions import BadRequestKeyError

RESTAURANTS_ENDPOINT = '/api/v1/restaurants'
logger = logging.getLogger(__name__)
API_KEY = config('API_KEY')
MILES = 10


class RestaurantsResource(Resource):

    def get(self):

        restaurants = list()
        map_client = googlemaps.Client(API_KEY)

        try:
            lat, long, city = request.args['lat'], request.args['long'], request.args['city']

            if lat == '' or long == '' or city == '':
                return {'msg': 'Missing values lat, long, city'}, 400

        except BadRequestKeyError:
            return {'msg': 'Missing arguments lat, long, city'}, 400

        location = (lat, long)
        distance = 0 if self.miles_to_meters(MILES) <= 0 else self.miles_to_meters(MILES)

        try:
            response = map_client.places_nearby(location=location, type='restaurant', radius=distance)
        except:
            abort(500, msg=f'Invalid request')

        restaurants.extend(response.get('results'))

        if 'next_page_token' in response.keys():
            time.sleep(2)
            response = map_client.places_nearby(page_token=response['next_page_token'])
            restaurants.extend(response.get('results'))

        return Pagination.get_paginated_json(
            restaurants, f'http://localhost:5000{RESTAURANTS_ENDPOINT}?lat={lat}&long={long}&city={city}',
            len(restaurants), request.args.get('start', 1), request.args.get('limit', 20)
        )

    @staticmethod
    def miles_to_meters(miles):
        try:
            return miles * 1609.344
        except TypeError:
            return 0
