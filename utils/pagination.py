class Pagination:

    @staticmethod
    def get_paginated_json(restaurants_json, url, count, start, limit):

        start = int(start)
        limit = int(limit)

        data = dict()

        data['count'] = count
        data['start'] = None if not start else start
        data['limit'] = None if not limit else limit

        if start == 1:
            data['previous'] = None
        else:
            start_pagination = max(1, start - limit)
            limit_pagination = start - 1
            data['previous'] = url + '&start=%d&limit=%d' % (start_pagination, limit_pagination)

        if start + limit > count:
            data['next'] = None
        else:
            start_pagination = start + limit
            data['next'] = url + '&start=%d&limit=%d' % (start_pagination, limit)

        data['results'] = restaurants_json[(start - 1):(start - 1 + limit)]

        return data
